'use strict';

(function() {
    // Declare app level module which depends on views, and components
    angular.module('alMundo', [
        'ngRoute',
        'templates',
        'alMundo.factories',
        'alMundo.list',        
        'alMundo.globalComponents'
    ])
    .constant('globalConstants', {
        API_BASE_URL_CMS: 'http://localhost:3000',
        API_HOTELS: '/hotels',
        //Lightbox toggles
        LIGHTBOX_TOGGLES: {
        }
    }).
    config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.otherwise({redirectTo: '/list'});
    }]);
})();
