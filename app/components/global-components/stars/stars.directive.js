
'use strict';

(function() {
    angular
        .module('alMundo.globalComponents')
        .directive('alMundoStars', alMundoStars);

        alMundoStars.$inject = [];
        /* @ngInject */
         function alMundoStars() {
            return {
                restrict: 'E',
                templateUrl: 'global-components/stars/stars.directive.html',
                controller: AlMundoStars,
                controllerAs: 'alMundoStarsVm',
                scope: {
                    starsAmount: '@'
                },
                bindToController: true
            };
        }

        AlMundoStars.$inject = [];
        /* @ngInject */
        function AlMundoStars() {
            var vm = this;
            vm.startsArray = [];
            for (var i = 0; i < parseInt(vm.starsAmount); i++) {
                vm.startsArray.push(i);
            }
        }
})();
