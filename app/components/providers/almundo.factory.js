'use strict';

(function() {
    angular.module('alMundo.factories')
        .factory('alMundoFactory', alMundoFactory);

        alMundoFactory.$inject = ['$http', '$q', 'globalConstants'];
        /* @ngInject */

        function alMundoFactory($http, $q, globalConstants) {
            var resortsData = null;

            // API
            var service = {
                getResorts: getResorts,
                getResortSearchOptions: getResortSearchOptions,
            };

            return service;

            /////////////////////////////////////////////////////////////////////////////////////////////////
            function getResorts(query) {
                // Used $q to simplify response cache
                var queryString = resolveQuery(query);

                return $q(function(resolve, reject) {
                    if (resortsData) {
                        console.log('Resorts Service response cached returned', resortsData);
                        return resolve(resortsData);
                    }
                    return $http({
                        method: 'GET',
                        url: globalConstants.API_BASE_URL_CMS
                            + globalConstants.API_HOTELS
                            + (queryString ? queryString : '')
                    }).then(
                        function successCallback(response) {
                            console.log('Resorts endpoint response successful', response);
                            var resortsData = response.data;
                            return resolve(resortsData);
                        },
                        function errorCallback(response) {
                            console.error('Resorts data response error', response);
                            return reject({});
                        }
                    );
                })
            }

            function resolveQuery(query) {
                if(!query) return;
                var queryData = query.split('-');
                return '?sortBy=' + queryData[0] + '&sortCriteria=' + queryData[1]
            }

            function getResortSearchOptions() {
                return [
                    {label: 'Precio más bajo', value: 'price-ASC'},
                    {label: 'Precio más alto', value: 'price-DESC'},
                    {label: 'Más estrellas', value: 'stars-DESC'},
                    {label: 'Menos estrellas', value: 'stars-ASC'}
                ];
            }
        }
})();
