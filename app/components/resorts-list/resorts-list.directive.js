
'use strict';

(function() {
    angular
        .module('alMundo.resortsList')
        .directive('alMundoResortsList', resortsList);

        resortsList.$inject = [];
        /* @ngInject */
         function resortsList() {
            return {
                restrict: 'E',
                templateUrl: 'resorts-list/resorts-list.directive.html',
                controller: ResortsList,
                controllerAs: 'resortsListVm',
                scope: {
                }
            };
        }

        ResortsList.$inject = [
            '$rootScope'
        ];
        /* @ngInject */
        function ResortsList(
            rootScope
        ) {
            var vm = this;
            vm.resortData;

            rootScope.$on('UPDATE-RESORT-DATA', function (event, data) {
                vm.resortData = data;
            });
        }
})();
