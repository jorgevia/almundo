
'use strict';

(function() {
    angular
        .module('alMundo.resortsList')
        .directive('alMundoResortsSearch', resortsSearch);

        resortsSearch.$inject = [];
        /* @ngInject */
         function resortsSearch() {
            return {
                restrict: 'E',
                templateUrl: 'resorts-list/resorts-search.directive.html',
                controller: ResortsSearch,
                controllerAs: 'resortsSearchVm',
                scope: {
                }
            };
        }

        ResortsSearch.$inject = [
            'alMundoFactory',
            '$rootScope'
        ];
        /* @ngInject */
        function ResortsSearch(
            alMundoFactory,
            rootScope
        ) {
            var vm = this;
            vm.searchSelectValues = alMundoFactory.getResortSearchOptions();
            vm.searchOption = vm.searchSelectValues[0].value;
            vm.doSearch = doSearch;

            // Triggers first search on request
            doSearch('price-ASC');

            //////////////////////////////////////////
            function doSearch() {
                //do search
                var resortsListResult = alMundoFactory.getResorts(vm.searchOption).then(function(data) {
                    rootScope.$broadcast('UPDATE-RESORT-DATA', data);
                });
            }
        }
})();
