
'use strict';

(function() {

  angular.module('alMundo.list', ['ngRoute', 'alMundo.resortsList'])

    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/list', {
        templateUrl: 'list/list.html',
        controller: 'listCtrl'
      });
    }])
    
    .controller('listCtrl', [function() {
      
    }]);

})();
