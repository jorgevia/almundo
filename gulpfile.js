/*!
 * gulp
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del'),
    pump = require('pump'),
    inject = require('gulp-inject'),
    runSequence = require('run-sequence'),
    templateCache = require('gulp-angular-templatecache');

    gulp.task('default', function(callback) {
        runSequence('build', callback);
    });

    gulp.task('build', function (callback) {
        runSequence(
            'clean-dist',
            'styles',
            'tempĺate-cache',
            'copy-build',
            'index',
            'clean-build',
            'watch',
            callback
        );
    });

// Styles
gulp.task('styles', function() {
    return sass(['app/components/**/*.scss'])
      .pipe(autoprefixer('last 2 version'))
      .pipe(gulp.dest('build/css'))
      .pipe(minifycss())
      .pipe(gulp.dest('build/css/min'))
      .pipe(concat('bundle.css'))
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest('dist/css'))
      .pipe(notify({ message: 'Styles task complete' }));
});

// Clean
gulp.task('clean-build', function () {
    return del(['./build'], {force: true});
});

gulp.task('clean-dist', function () {
    return del(['./dist'], {force: true});
});

gulp.task('index',function(){
  var tplSrc = ['./dist/css/bundle.min.css', './dist/templates.js', './dist/js/bundle.min.js'];

  return gulp.src('./app/index.html')
    .pipe(inject(gulp.src(tplSrc), {ignorePath: 'dist'}))
    .pipe(gulp.dest('./dist'));
});

// Watch
gulp.task('watch', function() {
    gulp.watch(['./app/index.html', './app/app.js', './app/components/**/*'], ['build']);
});

gulp.task('copy-build', ['copy-vendor', 'uglify-concat-js']);

// Default task
gulp.task('default', function(cb) {
    runSequence('build', cb);
});

gulp.task('uglify-concat-js', function (cb) {
  pump([
        gulp.src(['./app/components/**/*module.js', './app/app.js', './app/components/**/*!(.module).js']),
        uglify(),
        concat('bundle.js'),
        rename({ suffix: '.min' }),
        gulp.dest('./dist/js')
    ],
    cb
  );
});

gulp.task('copy-vendor', function() {
  return gulp.src('./app/bower_components/**/*')
    .pipe(gulp.dest('./dist/bower_components'));
});

gulp.task('tempĺate-cache', function () {
  return gulp.src('./app/components/**/*.html')
    .pipe(templateCache({
        templateHeader: 'angular.module("<%= module %>", []).run(["$templateCache", function($templateCache) {'
    }))
    .pipe(gulp.dest('dist'));
});
