# `Proyecto Al Mundo para listar Hoteles`
Esta aplicación está realizada en Angular 1,5.

Para correr la aplicación:

1. Checkoutear repo
1. Correr yarn
1. npm start para iniciar servidor en puerto 8000 (https://localhost:8000)
1. correr gulp para crear directorio dist
1. La api que consume la aplicación está realizada con Node y Mongodb (Ver instrucciones en https://bitbucket.org/jorgevia/almundo-api)

Jorge Luis Viale
viale.jorge@gmail.com